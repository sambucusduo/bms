#ifndef __MYSQL_TABLE_CLASS_H_
#define __MYSQL_TABLE_CLASS_H_

#include "datatable_class.h"

#include <mysql/mysql.h>

class MYSQL_TAB {
private:
    MYSQL_RES *result;
    MYSQL_ROW *rows;

    long nrows;
public:
    /* constructors */
	MYSQL_TAB(MYSQL_RES *QueryResult);
    /* destructors */
	~MYSQL_TAB(void);

    
};

/* private */

/* public */
MYSQL_TAB::MYSQL_TAB(MYSQL_RES *QueryResult)
{
    MYSQL_ROW row;
    result = QueryResult;

    nrows = 0;
    while((row = mysql_fetch_row(result)) != 0) {
        rows[nrows++] = row;
    }
}

MYSQL_TAB::~MYSQL_TAB(void)
{

}


#endif
