#ifndef __MEASUREMENT_CLASS_H_
#define __MEASUREMENT_CLASS_H_

#include "batterycells_class.h"

struct MEASUREMENT
{
	            time_t timestamp;
	unsigned short int epd;
	         short int current;
	unsigned short int temp_cell;
	unsigned short int temp_mproc;
};

/* Measurements made of a battery cell */
class Measurement: public Mysql
{
private:
    const int MIN_TEMP;

	unsigned int nMeasurementsStored;
	unsigned short int _cellId;
	vector <string> _fieldNames;
	vector <MEASUREMENT> _measure;

	string error;
	Logbook *lb_entry;
public:
	/* Constructor */
	Measurement(unsigned int, vector <string>, vector <MEASUREMENT>);
	/* Destructor */
	~Measurement(void);

	string GetError(void);

	unsigned int Size(void);
	unsigned short int GetCell(void);
	time_t GetTimeStamp(unsigned int);
	unsigned short int GetEPD(unsigned int);
	short int GetCurrent(unsigned int);
	unsigned short int GetTempCell(unsigned int);
	unsigned short int GetTempProc(unsigned int);
	int GetMeasurement(unsigned int, MEASUREMENT &);

	string GetCellStr(void);
	string GetTimeStampStr(unsigned int);
	string GetEPDStr(unsigned int);
	string GetCurrentStr(unsigned int);
	string GetTempCellStr(unsigned int);
	string GetTempProcStr(unsigned int);
	
	vector <string> GetFields(void);
	vector <string> GetMeasurementValues(void);

	void Print(void);
};

/* private */

/* public */

/* Constructor */
Measurement::Measurement(unsigned int cell_id, vector <string> fieldnames, vector <MEASUREMENT> measure):MIN_TEMP(253)
{
	if((nMeasurementsStored = measure.size()) < 1) {
		error = "Number of measurements should be more or equal to 1";
		return;
	}
/*
	if(BatteryCells::Exists(cell_id) < 1) {
		error = "Cell ID does not exist";
		return;
	}
*/
	_measure = measure;
	_cellId = cell_id;

	_fieldNames = fieldnames;
}

/* Destructor */
Measurement::~Measurement(void)
{
	_measure.clear();
	_fieldNames.clear();
}

/* Get error message */
string Measurement::GetError(void)
{
	return error;
}

/* Number of measurements stored */
unsigned int Measurement::Size(void)
{
	if(!error.empty()) return FAILURE;

	return nMeasurementsStored;
}

/* Cell ID */
unsigned short int Measurement::GetCell(void)
{
	if(!error.empty()) return FAILURE;

	return _cellId;
}

/* Timestamp of index */
time_t Measurement::GetTimeStamp(unsigned int index)
{
	if(!error.empty()) return FAILURE;

	return _measure[index].timestamp;
}

/* Measured EPD of index */
unsigned short int Measurement::GetEPD(unsigned int index)
{
	if(!error.empty()) return FAILURE;

	return _measure[index].epd;
}

/* Measured current of index */
short int Measurement::GetCurrent(unsigned int index)
{
	if(!error.empty()) return FAILURE;

	return _measure[index].current;
}

/* Measured temperature of indexed cell */
unsigned short int Measurement::GetTempCell(unsigned int index)
{
	if(!error.empty()) return FAILURE;

	return _measure[index].temp_cell + MIN_TEMP;
}

/* Measured temperature of indexed microprocessor */
unsigned short int Measurement::GetTempProc(unsigned int index)
{
	if(!error.empty()) return FAILURE;

	return _measure[index].temp_mproc + MIN_TEMP;
}

/* Get index measurement package */
int Measurement::GetMeasurement(unsigned int index, MEASUREMENT &Measurement)
{
	if(!error.empty()) return FAILURE;

	if(index < 0 && index >= _measure.size()) return FAILURE;

	Measurement = _measure[index];

	return SUCCESS;
}

/* Gets Cell ID as string type */
string Measurement::GetCellStr(void)
{
	if(!error.empty()) return NULL;

	return StringPatch::ToString(_cellId);
}

/* Gets Timestamp of index as string type */
string Measurement::GetTimeStampStr(unsigned int index)
{
	if(!error.empty()) return NULL;

	return UnixTimeToSQLDate(_measure[index].timestamp);
}

/* Gets EPD of index as string type */
string Measurement::GetEPDStr(unsigned int index)
{
	if(!error.empty()) return NULL;

	return StringPatch::ToString(_measure[index].epd);
}

/* Gets current of index as string type */
string Measurement::GetCurrentStr(unsigned int index)
{
	if(!error.empty()) return NULL;

	return StringPatch::ToString(_measure[index].current);
}

/* Gets cell temperature of index as string type */
string Measurement::GetTempCellStr(unsigned int index)
{
	if(!error.empty()) return NULL;

	return StringPatch::ToString(_measure[index].temp_cell + MIN_TEMP);
}

/* Gets microprocessor temperature of index as string type */
string Measurement::GetTempProcStr(unsigned int index)
{
	if(!error.empty()) return NULL;

	return StringPatch::ToString(_measure[index].temp_mproc + MIN_TEMP);
}

/* Gets columns or fields of the table */
vector <string> Measurement::GetFields(void)
{
	if(!error.empty()) {
		vector <string> empty_string_array;
		return empty_string_array;
	}

	return _fieldNames;
}

/* Gets a list of values as string types */
vector <string> Measurement::GetMeasurementValues(void)
{
	vector <string> measurements;

	if(!error.empty()) {
		return measurements;
	}

	for(unsigned int index = 0; index < nMeasurementsStored; index++) {
		measurements.push_back(GetCellStr());
		measurements.push_back(GetTimeStampStr(index));
		measurements.push_back(GetEPDStr(index));
		measurements.push_back(GetCurrentStr(index));
		measurements.push_back(GetTempCellStr(index));
		measurements.push_back(GetTempProcStr(index));
	}

	return measurements;
}

/* Print the measurement on screen */
void Measurement::Print(void)
{
	cout << String::ImplodeJSON(_fieldNames, GetMeasurementValues()) << endl;
}

#endif
