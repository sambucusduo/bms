#ifndef __LOGBOOK_CLASS_H_
#define __LOGBOOK_CLASS_H_

#include "mysql_class.h"
#include "qualification_class.h"

class Logbook
{
private:
    Mysql *_database;
    string _tableName;
    bool broken;

    vector <string> _fieldNames;
public:
    /* Constructor */
    Logbook(Mysql *);
    /* Destructor */
    ~Logbook(void);

    bool isBroken(void);
    int Add(unsigned int, string, string, int);
};

/* private */

/* public */

/* Constructor */
Logbook::Logbook(Mysql *database)
{
    _database = database;

    Qualification _qualifications(_database);

    _tableName = "logs";

    if(broken = (_database->TableExists(_tableName) < 0 || _qualifications.isBroken())) {
        if(_database->TableExists(_tableName) < 0) {
    		cout << "Error: No table found with the name \"" << _tableName << "\"" << endl;
        } else {
            cout << "Qualifications data table is broken" << endl;
        }
    } else {
        vector <string> excluded;
        excluded.push_back("id");
        excluded.push_back("time");
        if(!database->GetFieldNames(_tableName, _fieldNames, excluded)) {
            cout << "Error: " << database->GetError();
        }
    }
}

/* Destructor */
Logbook::~Logbook(void)
{

}

/* Determens whether the process is broken */
bool Logbook::isBroken(void)
{
    return broken;
}

/* Adds logbook entry */
int Logbook::Add(unsigned int qualificationID, string title_subject, string logbook_message, int output = 0)
{
    if(isBroken()) return FAILURE;

    vector <string> values;
    values.push_back(StringPatch::ToString(qualificationID));
    values.push_back(title_subject);
    values.push_back(logbook_message);

    if(!_database->InsertValues(_tableName, _fieldNames, values)) {
        return FAILURE;
    }

    Qualification _qualifications(_database);

    if(output) {
        cout << _qualifications.GetQualificationFromID(qualificationID) << "(" << _qualifications.GetSymbolFromID(qualificationID) << ")" << ": ";
        cout << title_subject << " - " << logbook_message << endl;
    }

    lq->SetQualification(qualificationID);

    return SUCCESS;
}

#endif