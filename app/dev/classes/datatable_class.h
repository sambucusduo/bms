#ifndef __DATATABLE_CLASS_H_
#define __DATATABLE_CLASS_H_

#include "string_class.h"

/*
struct DATATABLE
{
    string fieldNames[];
    string values[];
    unsigned int column_size;
    unsigned int values_size;
};
*/

class DataTable
{
private:
    string _tableName;
	vector <string> _fieldNames, _values;
	unsigned int nColumns, nValues, nRows;

	string **_valuestable;

	string error;

    bool ValuesFit(unsigned int, unsigned int);
    void CreateValuesTable(void);
public:
	/* Constructor */
	DataTable(vector <string>, vector <string>, string);
	/* Destructor */
    ~DataTable(void);

    string GetError(void);

    string GetTableName(void);
    unsigned int GetRows(void);
    string **Get_valuestable(void);
    string GetFieldsString(void);
    string GetValuesString(void);
    void SetTableName(string);
	void PrintValues(string, string);
};

/* private */
bool DataTable::ValuesFit(unsigned int columnSize, unsigned int valueSize)
{
    return valueSize % columnSize == 0;
}

void DataTable::CreateValuesTable(void)
{
    _valuestable = new string*[nRows];

    for (unsigned int row = 0; row < nRows; row++) {
        _valuestable[row] = new string[nColumns];

        for (unsigned int col = 0; col < nColumns; col++) {
            _valuestable[row][col] = _values[row * col + col];
        }
    }
}

/* public */

/* Constructor */
DataTable::DataTable(vector <string> fieldNames, vector <string> values, string tableName = NULL)
{
	nColumns = fieldNames.size();
	nValues = values.size();

   	if(!ValuesFit(nColumns, nValues)) {
		error = "Number of values doesn't comply with the number of columns and thus invalid";
		return;
	}

    _tableName = tableName;
	nRows = nValues / nColumns;

	_fieldNames = fieldNames;
	_values = values;

    CreateValuesTable();
//    cout << "TEST" << endl;
}

/* Destructor */
DataTable::~DataTable(void)
{
    if(!error.empty()) return;

    for (unsigned int row = 0; row < nRows; row++) {
        delete [] _valuestable[row];
    }

    delete [] _valuestable;
    _valuestable = 0;
}

/* Gets a stored error */
string DataTable::GetError(void)
{
    return error;
}

/* Gets stored table name */
string DataTable::GetTableName(void)
{
    return _tableName;
}

/* Gets number of rows */
unsigned int DataTable::GetRows(void)
{
    if(!error.empty()) return 0;

    return nRows;
}

/* Gets pointer of the data table */
string **DataTable::Get_valuestable(void)
{
    if(!error.empty()) return NULL;

    return _valuestable;
}

/* Gets fields as an imploded string */
string DataTable::GetFieldsString(void)
{
    if(!error.empty()) return NULL;

    string fields[nColumns];
    
    for(unsigned int col = 0; col < nColumns; col ++) {
         fields[col] = _fieldNames[col];
    }

    String columns(String::ImplodeColumns(fields, nColumns, ", "));
    columns.Enclose();

    return columns.GetString();
}

/* Gets values as an imploded string */
string DataTable::GetValuesString(void)
{
    if(!error.empty()) return NULL;

    string row_str[nRows];
    for(unsigned int row = 0; row < nRows; row++) {
        String str(String::ImplodeValues(_valuestable[row], nColumns, ", "));
        str.Enclose();
        row_str[row] = str.GetString();
    }

    return String::Implode(row_str, nRows, ", ");
}

/* Sets or changes the table name */
void DataTable::SetTableName(string tablename)
{
    _tableName = tablename;
}

/* Prints values */
void DataTable::PrintValues(string seperator = "", string rowsep = "")
{
	if(!error.empty()) return;

    for (unsigned int row = 0; row < nRows; row++) {
        for (unsigned int col = 0; col < nColumns; col++) {
            cout << _valuestable[row][col] << (col < nColumns -1 ? seperator : rowsep);
        }
        cout << endl;
    }
}

#endif
