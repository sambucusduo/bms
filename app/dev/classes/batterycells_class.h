#ifndef __BATTERYCELLS_CLASS_H_
#define __BATTERYCELLS_CLASS_H_

#include "batterycell_class.h"

class BatteryCells : public Mysql//, public BatteryCell
{
private:
    const int MAX_CELLS;

    Mysql *_database;
	vector <BatteryCell> _battery_cells;
    Logbook *lb_entry;
public:
	/* Constructor */
	BatteryCells(Mysql *);
	/* Destructor */
	~BatteryCells(void);

    unsigned int GetNumberOfCells(void);
	unsigned int Add(BatteryCell *);
    unsigned int GetAvailableId(void);
    int GetLatestMountedCell(BatteryCell &);
    int Exists(unsigned int);
};

/* private */

/* public */

/* Constructor */
BatteryCells::BatteryCells(Mysql *database):MAX_CELLS(256)
{
    _database = database;
    lb_entry = new Logbook(_database);
}

/* Destructor */
BatteryCells::~BatteryCells(void)
{
    delete lb_entry;
    _battery_cells.clear();
//    delete _database;
}

/* Gets number of mounted battery cells */
unsigned int BatteryCells::GetNumberOfCells(void)
{
    return _battery_cells.size();
}

/* Adds a battery cell */
unsigned int BatteryCells::Add(BatteryCell *bc)
{
    if(GetNumberOfCells() >= MAX_CELLS) {
        lb_entry->Add(1, "Battery cell", "The system cannot hold more cells. The number of cells has been exceeded.", 1);
        return FAILURE;
    }

    if(_database->TableExists(bc->GetTableName()) < 0) {
		lb_entry->Add(1, "Table", "No table found with the name \"" + bc->GetTableName() + "\"", 1);
        return FAILURE;
    }

    if(!_database->InsertValues(bc->GetTableName(), bc->GetFields(), bc->GetValues())) {
        return FAILURE;
    }

    _database->FreeResult();

    if(!bc->GetId()) {
        BatteryCell cell;
        int cellid = GetLatestMountedCell(cell);
        bc->SetId(cell.GetId());
    }

    _battery_cells.push_back(*bc);

    return SUCCESS;
}

/* Gets the first available battery cell ID  */
unsigned int BatteryCells::GetAvailableId(void)
{
    unsigned int id = 1, index = 0, n = GetNumberOfCells();

    while(index < n) {
        if(_battery_cells[index].GetId() == id) {
            id++;
            index = 0;
        } else {
            index++;
        }
    }

    return id;
}

/* Get latest mounted battery cell ID */
int BatteryCells::GetLatestMountedCell(BatteryCell &bc)
{
    MYSQL_ROW row;

    string query = "SELECT `id` FROM `cells` ORDER BY `mounted` DESC";
    _database->Query(query);

    if((row = _database->FetchRow()) == 0) return FAILURE;

	_database->FreeResult();
//cout << "TEST " << strlen(row[0]) << endl;
    unsigned int cellid = atoi(row[0]);
    int index = Exists(cellid);
    if(index < 0) return FAILURE;

    bc = _battery_cells[index];
//    return cellid;
    return SUCCESS;
}

/* Check whether the Cell ID exists within the array of battery cells */
int BatteryCells::Exists(unsigned int cellid)
{
    unsigned int found = 0, index = 0;
    while(!found && index < GetNumberOfCells()) {
        found = (_battery_cells[index++].GetId() == cellid);
    }

    return (found ? index : 0) -1;
}

#endif
