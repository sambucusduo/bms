/* 1. Select database */

USE bms_db;



/* 2. Drop existing tables in database */

DROP TABLE IF EXISTS logs;
DROP TABLE IF EXISTS qualifications;
DROP TABLE IF EXISTS measurements;
DROP TABLE IF EXISTS cells;
DROP TABLE IF EXISTS settings;
DROP TABLE IF EXISTS domains;



/* 3. Create tables */

CREATE TABLE IF NOT EXISTS updates (
    `id` int(11) NOT NULL AUTO_INCREMENT,
    `updated_at` timestamp DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,

    KEY(`updated_at`),
    PRIMARY KEY(`id`)
);


CREATE TABLE domains (
    `id` int(11) NOT NULL UNIQUE AUTO_INCREMENT,
    `subject` varchar(20) NOT NULL UNIQUE,
    `min` int(4) NOT NULL,
    `max` int(4) NOT NULL,
    `tolerance` int(4) NOT NULL DEFAULT 0,

    PRIMARY KEY(`id`)
);


CREATE TABLE settings (
    `id` int(11) NOT NULL UNIQUE AUTO_INCREMENT,
--    `composition` varchar(20) NOT NULL,
    `setting` int(11) NOT NULL,
    `domain` int(11) NOT NULL,

    INDEX(`setting`),
    PRIMARY KEY(`id`),
    FOREIGN KEY(`domain`) REFERENCES domains(`id`),
    UNIQUE KEY(`setting`, `domain`)
);


CREATE TABLE cells (
    `id` int(4) NOT NULL UNIQUE AUTO_INCREMENT,
    `epd_id` int(4) NOT NULL UNIQUE, -- mV
    `active` int(1) NOT NULL,
    `mounted` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `setting` int(11) NOT NULL,

    PRIMARY KEY(`id`)
);


CREATE TABLE measurements (
    `id` int(11) NOT NULL UNIQUE AUTO_INCREMENT,
    `cell` int(4) NOT NULL,
    `time` timestamp NOT NULL,
    `epd` int(4) NOT NULL, -- in mV
    `current` int(4) NOT NULL, -- in mA
    `temp_cell` int(4) NOT NULL, -- in Kelvin
    `temp_microproc` int(4) NOT NULL, -- in Kelvin

    PRIMARY KEY(`id`),
    FOREIGN KEY(`cell`) REFERENCES cells(`id`)
);



CREATE TABLE qualifications (
    `id` int(11) NOT NULL UNIQUE AUTO_INCREMENT,
    `qualification` varchar(20) NOT NULL UNIQUE,
    `symbol` varchar(3) NOT NULL UNIQUE,
    -- `weight` int(2) NOT NULL,

    PRIMARY KEY(`id`)
);



CREATE TABLE logs (
    `id` int(11) NOT NULL UNIQUE AUTO_INCREMENT,
    `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `qualification` int(11) NOT NULL,
    `subject` varchar(100) NOT NULL,
    `log` text NOT NULL,

    PRIMARY KEY(`id`),
    FOREIGN KEY(`qualification`) REFERENCES qualifications(`id`)
);


/* 4. Insert known data into tables */

SET sql_mode = '';

INSERT INTO updates () VALUES
();

INSERT INTO qualifications (`qualification`, `symbol`) VALUES
('Imminent', 'X'),
('Error', '*'),
('Warning', '!'),
('Information', 'i'),
('Issue', ';'),
('Problem', '|'),
('Halted', '#'),
('Question', '?');

INSERT INTO domains (`subject`, `min`, `max`, `tolerance`) VALUES
('epd', 256, 426, 1),
('current', -256, 256, 1),
('temp_cell', 253, 363, 1),
('temp_microproc', 253, 363, 1);

INSERT INTO settings (`setting`, `domain`) VALUES
(1, 1),
(1, 2),
(1, 3),
(1, 4);