#ifndef __QUALIFICATIONS_CLASS_H_
#define __QUALIFICATIONS_CLASS_H_

#include "mysql_class.h"
#include "string_class.h"

class LatestQualification
{
private:
    unsigned int _qualification;
public:
    LatestQualification(void)
    {
        _qualification = 0;
    }

    void SetQualification(unsigned int q)
    {
        _qualification = q;
    }

    unsigned int GetQualification(void)
    {
        return _qualification;
    }
} *lq = new LatestQualification();

class Qualification
{
private:
    Mysql *_database;
    string _tableName;
    vector <unsigned int> _ids;
    vector <string> _sids;
    vector <string> _qualifications;
    vector <string> _symbols;
    bool broken;

    void GetQualifications(void);
    int GetIndex(string, vector <string>, unsigned int);
public:
    /* Constructor */
    Qualification(Mysql *);
    /* Destructor */
    ~Qualification();

    bool isBroken(void);
    int IdExists(unsigned int);
    int SeekQualification(string, unsigned int);
    int SeekSymbol(string, unsigned int);
    unsigned int GetIDFromQualification(string);
    unsigned int GetIDFromSymbol(string);
    string GetQualificationFromID(unsigned int);
    string GetQualificationFromSymbol(string);
    string GetSymbolFromID(unsigned int);
    string GetSymbolFromQualification(string);
};

/* private */

/* Gets the data stored in the Mysql database */
void Qualification::GetQualifications(void)
{
    string query = "SELECT id, qualification, symbol FROM " + _tableName + " ORDER BY id";
    _database->Query(query);

    MYSQL_ROW row;
    while((row = _database->FetchRow()) != 0) {
        unsigned int i = atoi(row[0]);
        _ids.push_back(i);
        _sids.push_back(row[0]);
        _qualifications.push_back(row[1]);
        _symbols.push_back(row[2]);
    }

    _database->FreeResult();
}

/* Seeks a string needle in a string array and returns the position of the haystack, otherwise it returns -1 */
int Qualification::GetIndex(string needle, vector <string> haystack, unsigned int offset = 0)
{
    unsigned int found = 0, index = offset, n = haystack.size();
    while(!found && index < n) {
        if(found = (haystack[index].compare(needle) == 0)) {
            index++;
        }
    }

    return (found ? index : 0) -1;
}

/* public */

/* Constructor */
Qualification::Qualification(Mysql *database)
{
    _database = database;
    _tableName = "qualifications";

    if(broken = (_database->TableExists(_tableName) < 0)) {
        cout << "Error: No table found with the name \"" << _tableName << "\"" << endl;
    } else {
        GetQualifications();
    }
}

/* Destructor */
Qualification::~Qualification()
{
    _ids.clear();
    _sids.clear();
    _qualifications.clear();
    _symbols.clear();
}

/* Determens whether the process is broken */
bool Qualification::isBroken(void)
{
    return broken;
}

/* Seeks the ID of a qualification */
int Qualification::IdExists(unsigned int needle)
{
    return GetIndex(StringPatch::ToString(needle), _sids, 0);
}

/* Seeks the qualification */
int Qualification::SeekQualification(string needle, unsigned int offset = 0)
{
    return GetIndex(needle, _qualifications, offset);
}

/* Seeks a symbol of a qualification */
int Qualification::SeekSymbol(string needle, unsigned int offset = 0)
{
    return GetIndex(needle, _symbols, offset);
}

/* Gets the ID of a qualification */
unsigned int Qualification::GetIDFromQualification(string qualification)
{
    unsigned int searchedIndex = SeekQualification(qualification);
    return searchedIndex < 0 ? searchedIndex : _ids[searchedIndex];
}

/* Gets the ID of a symbol */
unsigned int Qualification::GetIDFromSymbol(string symbol)
{
    unsigned int searchedIndex = SeekSymbol(symbol);
    return searchedIndex < 0 ? searchedIndex : _ids[searchedIndex];
}

/* Gets qualification from the ID */
string Qualification::GetQualificationFromID(unsigned int id)
{
    unsigned int searchedIndex = IdExists(id);
    return searchedIndex < 0 ? "" : _qualifications[searchedIndex];
}

/* Gets qualification from a symbol */
string Qualification::GetQualificationFromSymbol(string symbol)
{
    unsigned int searchedIndex = SeekSymbol(symbol);
    return searchedIndex < 0 ? "" : _qualifications[searchedIndex];
}

/* Gets a symbol from the ID */
string Qualification::GetSymbolFromID(unsigned int id)
{
    unsigned int searchedIndex = IdExists(id);
    return searchedIndex < 0 ? "" : _symbols[searchedIndex];
}

/* Gets a symbol by its qualification */
string Qualification::GetSymbolFromQualification(string qualification)
{
    unsigned int searchedIndex = SeekQualification(qualification);
    return searchedIndex < 0 ? "" : _symbols[searchedIndex];
}

#endif