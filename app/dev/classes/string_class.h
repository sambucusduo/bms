#ifndef __EXTRA_STRING_FUNCTIONS_H_
#define __EXTRA_STRING_FUNCTIONS_H_

#include <sstream>
#include <string>

#include <bits/stdc++.h>
#include <iostream>

using namespace std;

/* Converts (integer) numbers to the string type */
namespace StringPatch
{
    template <typename T> std::string ToString(const T& n)
    {
        std::ostringstream stm;
        stm << n;
        return stm.str();
    }
}

class String
{
private:
    string str;
public:
    /* constructor */
    String(string);
    /* constructor */
    String(char *, unsigned int);
    /* destructor */
    ~String();

    string GetString(void);
    bool isNumber(void);
    void Enclose(string enclosure);
    void JSONEnclose(void);
    static string Implode(string *, unsigned int, string);
    static string ImplodeColumns(string *, unsigned int, string, string);
    static string ImplodeValues(string *, unsigned int, string);
    static string ImplodeJSON(vector <string>, vector <string>);
    static void PrintList(vector <string>);
};

/* private */

/* public */

/* Default constructor */
String::String(string s)
{
    str = s;
}

/* Convert char* to string */
String::String(char *s, unsigned int size)
{
    str = ""; 
    for (unsigned int i = 0; i < size; i++) { 
        str += s[i]; 
    } 
}

/* Destructor */
String::~String()
{

}

/* Gets stored string */
string String::GetString(void)
{
    return str;
}

/* Validates whether string is numeric */
bool String::isNumber(void)
{
    if(str.empty()) return false;

    return (str.find_first_not_of( "0123456789" ) == string::npos ? true : false);
}

/* Encloses string with a given string. An empty argument, encloses with ( and ) */
void String::Enclose(string enclosure = "")
{
    if(enclosure.empty()) {
        str = "(" + str + ")";
    } else {
        str = enclosure + str + enclosure;
    }
}

/* Encloses string with { and } */
void String::JSONEnclose(void)
{
    str = "{" + str + "}";
}

/* Implodes an array of strings with a given string to glue. */
string String::Implode(string *arr, unsigned int size, string glue)
{
    string s = "";
    if(size > 1) {
        s = Implode(arr, size-1, glue) + glue;
    }

    return s + arr[size-1];
}

/* Implodes an array of strings with a given string to glue. Each value in the array gets a ` to enclose */
string String::ImplodeColumns(string *columns, unsigned int size, string glue, string tablename = "")
{
    string s = "";
    if(size > 1) {
        s = ImplodeColumns(columns, size-1, glue, tablename) + glue;
    }

    String str_manip(columns[size-1]);
    str_manip.Enclose("`");
    
    return s + (tablename.empty() ? "" : (tablename + ".")) + str_manip.GetString();
}

/* Implodes an array of strings with a given string to glue. Each value in the array gets a propper enclosure. */
string String::ImplodeValues(string *values, unsigned int size, string glue)
{
    string s = "";
    if(size > 1) {
        s = ImplodeValues(values, size-1, glue) + glue;
    }

    if(values[size-1].empty()) {
        values[size-1] = "NULL";
    }
    
    String str_manip(values[size-1]);
    if(!str_manip.isNumber() && values[size-1].compare("NULL") != 0) {
        str_manip.Enclose("'");
    }

    return s + str_manip.GetString();
}

/* Implodes the key and value arrays to one JSON string */
string String::ImplodeJSON(vector <string> keys, vector <string> values)
{
    if(keys.size() != values.size()) return NULL;
    
    string result = "";
    String key(keys.back());
    String value(values.back());
    key.Enclose("\"");
    if(!value.isNumber()) {
        value.Enclose("\"");
    }

    if(keys.size() > 1) {
        keys.pop_back();
        values.pop_back();

        result = ImplodeJSON(keys, values) + ",";
    }

    return result + key.GetString() + ":" + value.GetString();
}

void String::PrintList(vector <string> stringlist)
{
    unsigned int n = stringlist.size();
    for(unsigned int i = 0; i < n; i++) {
        cout << stringlist[i] << endl;
    }
}

#endif