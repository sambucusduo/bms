#ifndef __DOMAIN_SETTING_CLASS_H_
#define __DOMAIN_SETTING_CLASS_H_

#include "mysql_class.h"

#define WITHIN_DOMAIN 0
#define WITHIN_TOLERANCE -1
#define OUT_OF_DOMAIN -2

#define MAX_TOLERANCE 1000
#define MIN_TOLERANCE -1000

class DomainSetting
{
private:
    unsigned int _id;
    string _subject;
	int _min;
	int _max;
	int _tolerance;

    /* processed values */
    int _size;
    double _percentage;
    double _tolerance_field;
public:
    /* Constructor */
    DomainSetting(unsigned int, string, int, int, int);
    /* Destructor */
    ~DomainSetting();

    unsigned int GetId(void);
    string GetSubject(void);
    int GetMin(void);
    int GetMax(void);
    int GetTolerance(void);
    int Size(void);
    bool WithinDomain(int);
    bool WithinTolerance(int);
    bool OutOfDomain(int);
    int Value(int);
};

/* Constructor */
DomainSetting::DomainSetting(unsigned int id, string subject, int min, int max, int tolerance = 0)
{
    _id = id;
    _subject = subject;

    if(min <= max) {
        _min = min;
        _max = max;
        _tolerance = (min == max ? 0 : tolerance);
    } else {
        _min = max;
        _max = min;
        _tolerance = tolerance;
    }

    if(_tolerance > MAX_TOLERANCE) _tolerance = MAX_TOLERANCE;
    if(_tolerance < MIN_TOLERANCE) _tolerance = MIN_TOLERANCE;

    _size = _max - _min;
    _percentage = _tolerance / MAX_TOLERANCE;
    _tolerance_field = (_size / 2) * _percentage;
}

/* Destructor */
DomainSetting::~DomainSetting()
{

}

/* Gets the ID */
unsigned int DomainSetting::GetId(void)
{
    return _id;
}

/* Gets the domains' subject */
string DomainSetting::GetSubject(void)
{
    return _subject;
}

/* Gets the domains' minimal value */
int DomainSetting::GetMin(void)
{
    return _min;
}

/* Gets the domains' maximal value */
int DomainSetting::GetMax(void)
{
    return _max;
}

/* Gets the domains' tolerance */
int DomainSetting::GetTolerance(void)
{
    return _tolerance;
}

/* Gets the domains' size */
int DomainSetting::Size(void)
{
    return _size;
}

/* Checks whether the value is within the given domain */
bool DomainSetting::WithinDomain(int value)
{
    return value >= _min && value <= _max;
}

/* Checks whether the value is within the given tolerance */
bool DomainSetting::WithinTolerance(int value)
{
    if(_tolerance == 0) return false;
    if(WithinDomain(value) && _tolerance == MAX_TOLERANCE) return true;

    return _tolerance > 0 ? 
        (value > _min && value < _min + _tolerance_field) || (value > _max + _tolerance_field && value < _max) :
        (value > _min + _tolerance_field && value < _min) || (value > _max && value < _max + _tolerance_field);
}

/* Checks whether the value is outside the given domain */
bool DomainSetting::OutOfDomain(int value)
{
    bool indomain = WithinDomain(value);
    return _tolerance > 0 ? !indomain : !indomain && !WithinTolerance(value);
}

/* Values a value genarally in perspective to its domain */
int DomainSetting::Value(int value)
{
    if(OutOfDomain(value)) return OUT_OF_DOMAIN;

    if(WithinTolerance(value)) return WITHIN_TOLERANCE;

    return WITHIN_DOMAIN;
}

#endif