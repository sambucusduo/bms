#ifndef __SETTINGS_CLASS_H_
#define __SETTINGS_CLASS_H_

#include "domain_setting_class.h"

class Settings
{
private:
    Mysql *_database;
    unsigned int _settingId;
    vector <DomainSetting> _domain;
public:
    /* Constructor */
    Settings();
    Settings(Mysql *, unsigned int);
    /* Destructor */
    ~Settings();

    int GetDomain(string);
    int Value(string, int);
    unsigned int GetSettingId(void);
};

/* Constructor */
Settings::Settings()
{
    
}

/* Constructor */
Settings::Settings(Mysql *database, unsigned int set)
{
    vector <string> fieldnames, exclude;
    _database = database;
    _settingId = set;
    string tablename("domains");

    if(!_database->GetFieldNames(tablename, fieldnames, exclude)) return;
    
    unsigned int n = fieldnames.size();
    string columns[n];
    for(unsigned int i = 0; i < n; i++) {
        columns[i] = fieldnames[i];
    }

    string query = "SELECT " + String::ImplodeColumns(columns, n, ", ", "`" + tablename + "`");
    query += " FROM " + tablename;
    query += " JOIN settings ON " + tablename + ".id = settings.domain ";
    query += " WHERE settings.setting = " + StringPatch::ToString(_settingId);

    _database->Query(query);

    MYSQL_ROW row;
    while((row = _database->FetchRow()) != 0) {
        DomainSetting domain(atoi(row[0]), row[1], atoi(row[2]), atoi(row[3]), atoi(row[4]));
        _domain.push_back(domain);
    }
}

/* Destructor */
Settings::~Settings()
{

}

/* Gets the index of a domain from a unique domain subject */
int Settings::GetDomain(string subject)
{
    unsigned int pos = 0, n = _domain.size(), found = 0;

    while(!found && pos < n) {
        found = (_domain[pos++].GetSubject() == subject);
    }

    return (found ? pos : 0) -1;
}

/* Values a value from a subjects' domain */
int Settings::Value(string subject, int value)
{
    int index = GetDomain(subject);
    if(index < 0) return FAILURE;

    return _domain[index].Value(value);
}

/* Gets the set ID */
unsigned int Settings::GetSettingId(void)
{
    return _settingId;
}

#endif