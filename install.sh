#!/bin/bash

# To get this installation script from Git-repo
# wget https://bitbucket.org/sambucusduo/bms/src/5f6d7060abe498ff79403afe3a340a88b144a8d6/install.sh | ~/ && sudo chmod 730 ~/install.sh
#
#
# Description:
# This Bash script is designed to load on Ubuntu MATE 18.04 for the ARM64 chip (Raspberry PI 3/3+/3B/3B+/4/4B) mounted on a multi cell battery.
# The script will minimize the system and install a localhost, the C/C++ program and the web application.
# 
#
# List of tasks (some are syntaxed as GUI tasks by hand):
#
# PHASE 1/3; Remove unnecessary stuff
# ===================================
#
# 0. Download archive in the background; Update and full upgrade
# 1. Delete programs and backgrounds. Choose a solid neutral color (antraciet)
# 2. Menu > Control Center > Users and Groups
# 		Password:
# 			Klik "Change..."
# 			Vink "Don't ask for password on login" aan
# 			Klik "OK" (Typ wachtwoord)
# 3. Menu > Control Center > MATE Tweak
# 		Desktop: Vink "Show Desktop Icons" uit
# 		Panel: "Redmond"; "Enable HUD"
# 		Windows: Vink "Enable animations", "Enable windows snapping" en "Do not auto-maximize new windows" uit
#       (Delete panels, desktop icons)
# 4. Menu > Control Center > Sound
# 		Vink "Enable window and button sounds" uit
# 5. Menu > Control Center > Main Menu
# 		Vink alle menu's uit
#       (Zie punt 3)
# 6. Menu > Control Center > Screensaver
# 		Vink "Activate screensaver when computer is idle" uit
#
#
# PHASE 2/3; Setup localhost and applications
# ===========================================
#
# 7. Menu > Control Center > Startup Applications
# 		a. Add:
# 			Name: Mozilla Firefox
# 			Command: firefox
# 			Comment: Open Source Webbrowser
#		b. Landingspagina: localhost
#		c. addon: Auto Fullscreen (See: https://addons.mozilla.org/en-US/firefox/addon/autofullscreen/)
#       (Extract profile)
# 8. Open Terminal
# 		Edit > Profile Preferences > Scrolling > Vink "Unlimited" aan
#       (dconf-editor: No schema available)
# 9. Install packages
#       - localhost
#       - mysql & php
#       - mysql secure installation
#           Type "y" (Validate Password Plugin)
#           Type "2" (STRONG)
#           Type new password
#           Re-enter new password
#           Type "y" (Continue?)
#           Type "y" (Remove anonymous users?)
#           Type "y" (Disallow root login remotely?)
#           Type "y" (Remove test database and access to it?)
#           Type "y" (Reload privilege tables now?)
# 10. Update en upgrade
# 11. Extract application
# 12. Set rights for localhost
# 13. To do: Setup crontabs
# 14. Extract web application
# 15. Load database tables
#
#
# PHASE 3/3; Clean up
# ===================
#
# 16. Clean up
#
# -----------------------------------------------------------------------------------------------------------
# So far the scheme, now the code. (Note that the order could be different):

# Get date and time of starting the bash script
# See: https://unix.stackexchange.com/questions/387010/bash-calculate-the-time-elapsed-between-two-timestamps
start=$(date "+%s")
isodate=$(date +"%FT%T%z")
fulldate=$(date +"%A %d %B %Y")

# Change to default directory
cd ~
appdir="/home/$(whoami)/.app"
logdir="$appdir/log"
installog="$logdir/install.log"
wgetlog="$logdir/wget"

lightdmgreeter="/usr/share/lightdm/lightdm.conf.d/60-lightdm-gtk-greeter.conf"

# Get temperature of CPU
# See: https://www.cyberciti.biz/faq/linux-find-out-raspberry-pi-gpu-and-arm-cpu-temperature-command/
# See: https://www.raspberrypi.org/forums/viewtopic.php?t=34994
# See: https://stackoverflow.com/questions/17368067/length-of-string-in-bash
# See: https://linuxize.com/post/bash-functions/
cputempvalval=0
cputemp=0
cpuTempValRec() {
    cputempval=$(cat /sys/class/thermal/thermal_zone0/temp)
}

printcputemp() {
    cpuTempValRec
    local len=${#cputempval}
    local unit=$((cputempval))

    if [[ $# -eq 1  &&  $1 =~ ^C(elsius)?$ ]]; then
        unit=$((cputempval)) # Celsius
    else
        unit=$((cputempval + 273150)) # Kelvin
    fi
    
    if (( $len > 3  &&  $len <= 5 )); then
        local cputempval_int=$(($unit / 1000))
        local cputempval_frac=$(($unit % 1000))

        cputemp="$cputempval_int.$cputempval_frac"
        result=0
    elif (( $len > 0 )); then
        sudo echo -e "$(date '+%F %T') $(($(date '+%s') - $start))s:   CPU temperature is of scale and continue working may damage the CPU" | tee -a "$installog"
        result=2
    else
        echo "null"
        result=1
    fi

    return $result
}

TempOutOfBoundries() {
    printcputemp Celsius
    if [[ $? ]]; then
        if [ $# -ge 2 ]  &&  [ $1 -gt 999 ]  &&  [ $1 -le 99999 ]  &&   [ $cputempval -ge $1 ]   && [ $2 -ge 0 ]; then
            sudo echo -e "$(date '+%F %T') $(($(date '+%s') - $start))s:   Cooling down from $cputemp°C to below $(($1 / 1000)).$(($1 % 1000))°C" | tee -a "$installog"
            i=0
            while [ $cputempval -ge $1 ]; do
                sleep $2
                printcputemp Celsius
                echo "CPU temperature:  $cputemp°C"
                ((i=i+1))
            done
        fi
        sudo echo -e "\n\n$(date '+%F %T') $(($(date '+%s') - $start))s:   CPU temperature:  $cputemp°C (Cooldown time: $((i * $2))s)\n\n" | tee -a "$installog"
    elif [ $? -eq 2  &&  $# -eq 4  &&  "$3" =~ "^(reboot|shutdown)$"  &&  $4 -ge 0 ]]; then
        if [ "$3" -eq "shutdown" ]; then
            sudo echo -e "$(date '+%F %T') $(($(date '+%s') - $start))s:   Shutting down..." | tee -a "$installog"
            sleep $4
            sudo shutdown now
        else
            sudo echo -e "$(date '+%F %T') $(($(date '+%s') - $start))s:   Rebooting..." | tee -a "$installog"
            sleep $4
            sudo reboot now
        fi
    else
        sudo echo -e "$(date '+%F %T') $(($(date '+%s') - $start))s:   Error: Invalid arguments given to TempOutOfBoundries()" | tee -a "$installog"
        exit
    fi
}

sudo rm -rf $appdir
mkdir $appdir
sudo rm -rf $logdir
mkdir -p $wgetlog

printcputemp Celsius

# Installation log file
# See: https://stackoverflow.com/questions/418896/how-to-redirect-output-to-a-file-and-stdout
# See: https://www.howtogeek.com/299219/how-to-save-the-output-of-a-command-to-a-file-in-bash-aka-the-linux-and-macos-terminal/
uname -a >> $installog
sudo echo -e "CPU Temperature $cputemp°C\n
Installation process started at $fulldate, $isodate\n
===============================================================\n" >> $installog
sudo echo -e "$(date '+%F %T') $(($(date '+%s') - $start))s:   $wgetlog created"  | tee -a "$installog"

# See: https://linuxacademy.com/blog/linux/conditions-in-bash-scripting-if-statements/
if [ -n "$(whoami | grep root)" ]
then
    sudo echo -e "$(date '+%F %T') $(($(date '+%s') - $start))s:   Not running as root. Exiting" | tee -a "$installog"
    exit 1
fi
echo "$(date '+%F %T') $(($(date '+%s') - $start))s:   Running as root" | tee -a "$installog"

desktop=$(echo $DESKTOP_SESSION | tr '[:upper:]' '[:lower:]')
sudo echo -e "$(date '+%F %T') $(($(date '+%s') - $start))s:   Desktop in use: $desktop" | tee -a "$installog"

# Exit when no supported desktop has been found
# Example:
#if [ "$desktop" != "ubuntu" ] && [ "$desktop" != "lubuntu" ] && [ "$desktop" != "mate" ]

if [ "$desktop" != "mate" ]
then
    sudo echo -e "$(date '+%F %T') $(($(date '+%s') - $start))s:   No supported desktop found. Exiting" | tee -a "$installog"
    exit 1
fi

# Set root password
sudo echo -e "$(date '+%F %T') $(($(date '+%s') - $start))s:   $(sudo passwd root)" | tee -a "$installog"

TempOutOfBoundries 55000 20 shutdown 10

# Avoid system to lock or falls asleep, etc. during installation
# See: https://superuser.com/questions/267637/how-can-i-change-the-time-before-the-system-locks
# See: https://askubuntu.com/questions/47311/how-do-i-disable-my-system-from-going-to-sleep
# See: https://askubuntu.com/questions/1048774/disabling-lock-screen-18-04
# See: https://askubuntu.com/questions/177348/how-do-i-disable-the-screensaver-lock
sudo systemctl mask sleep.target suspend.target hibernate.target hybrid-sleep.target
gsettings set org.mate.lockdown disable-lock-screen true
gsettings set org.mate.screensaver idle-activation-enabled false
gsettings set org.mate.session idle-delay 0
gsettings set org.gnome.desktop.lockdown disable-lock-screen true
gsettings set org.gnome.desktop.session idle-delay 0
gsettings set org.gnome.desktop.screensaver idle-activation-enabled false
gsettings set org.gnome.desktop.screensaver lock-enabled true
gsettings set org.gnome.desktop.screensaver ubuntu-lock-on-suspend false
sudo echo -e "$(date '+%F %T') $(($(date '+%s') - $start))s:   Suspend disabled" >> $installog

# See: https://askubuntu.com/questions/46627/how-can-i-make-a-script-that-opens-terminal-windows-and-executes-commands-in-the
sudo apt install htop -y && mate-terminal -e htop
sudo echo -e "$(date '+%F %T') $(($(date '+%s') - $start))s:   htop installed and opened in mate-terminal" >> $installog

TempOutOfBoundries 55000 20 shutdown 10

# Download BMS installation archive
sudo echo -e "$(date '+%F %T') $(($(date '+%s') - $start))s:   Downloading archive in background. See log file: $wgetlog/bms-install-archive.log" >> $installog
wget -bo  $wgetlog/bms-install-archive.log http://dev.mindcontrolled.nl/dartello/bms/bms.tar.gz -O bms.tar.gz --tries=3 --no-check-certificate | tee -a "$installog"

# Set MATE desktop to Redmond theme
mate-tweak --layout redmond
sleep 20 # Wait to finish the layout setting
sudo echo -e "$(date '+%F %T') $(($(date '+%s') - $start))s:   Redmond layout set" >> $installog

TempOutOfBoundries 55000 20 shutdown 10

# Update and upgrade
# See: https://askubuntu.com/questions/118025/bypass-the-yes-no-prompt-in-apt-get-upgrade
sudo apt update && sudo apt full-upgrade --fix-missing --force-yes -y
sudo echo -e "$(date '+%F %T') $(($(date '+%s') - $start))s:   System updated and upgraded" >> $installog

TempOutOfBoundries 55000 20 shutdown 10

sudo sed -i 's/enabled=1/enabled=0/g' /etc/default/apport
sudo echo -e "$(date '+%F %T') $(($(date '+%s') - $start))s:   Disable Ubuntu error reports" >> $installog



# Clear home directory
sudo rm -r ~/Desktop/
sudo rm -r ~/Documents
sudo rm -r ~/Downloads/
sudo rm -r ~/Music
sudo rm -r ~/Pictures
sudo rm -r ~/Templates
sudo rm -r ~/Videos
sudo echo -e "\n\n$(date '+%F %T') $(($(date '+%s') - $start))s:   Home directory cleared. Purging packages..." >> $installog


# Remove applications
# See: https://ubuntu-mate.community/t/ultimate-remove-thread-remove-unnecessary-packages-from-ubuntu-mate/18962
sudo echo -e "$(date '+%F %T') $(($(date '+%s') - $start))s:   Start purging process (1/5)" | tee -a "$installog"
sudo apt purge --remove --yes colord colord-data yelp mate-user-guide ubuntu-mate-guide bluez-cups apturl seahorse folder-color-common folder-color-caja ubuntu-report popularity-contest vlc-plugin-notify xul-ext-ubufox caja-gtkhash gtkhash libb2-1 exfat-fuse ifuse ippusbxd libimobiledevice-utils transmission-common transmission-gtk deja-dup deja-dup-caja libreoffice-style-tango libreoffice-style-galaxy ure fonts-indic fonts-beng fonts-gubbi fonts-knda fonts-gujr fonts-gujr-extra fonts-lohit-telu fonts-telu fonts-guru fonts-lohit-gujr fonts-lohit-guru fonts-lohit-knda fonts-mlym fonts-orya fonts-taml fonts-lohit-beng-assamese fonts-lohit-deva fonts-deva fonts-lohit-taml-classical
TempOutOfBoundries 55000 20 shutdown 10
sudo echo -e "$(date '+%F %T') $(($(date '+%s') - $start))s:   Start purging process (2/5)" | tee -a "$installog"
sudo apt purge --remove --yes thunderbird thunderbird-gnome-support xul-ext-gdata-provider xul-ext-lightning xul-ext-calendar-timezones gir1.2-rb-3.0 bluez bluez-obexd pulseaudio-module-bluetooth rhythmbox-plugins librhythmbox-core10 rhythmbox rhythmbox-data rhythmbox-plugin-alternative-toolbar rhythmbox-plugin-cdrecorder rhythmbox-plugins duplicity shotwell shotwell-common libsane-common libsane1 libsane-hpaio gnome-video-effects python3-brlapi python3-cups python3-cupshelpers python-samba samba-common vlc-plugin-samba smbclient brltty brltty-x11 xbrlapi libavahi-compat-libdnssd1 caja-seahorse
TempOutOfBoundries 55000 20 shutdown 10
sudo echo -e "$(date '+%F %T') $(($(date '+%s') - $start))s:   Start purging process (3/5)" | tee -a "$installog"
sudo apt purge --remove --yes printer-driver-pxljr printer-driver-c2esp printer-driver-ptouch printer-driver-sag-gdi printer-driver-brlaser printer-driver-c2esp printer-driver-foo2zjs printer-driver-gutenprint printer-driver-hpcups printer-driver-m2300w printer-driver-min12xxw printer-driver-pnm2ppa printer-driver-postscript-hp printer-driver-splix cups-bsd geoclue-2.0 geoclue-geonames geoip-database libzeitgeist-1.0-1 libzeitgeist-1.0-1-dbg libzeitgeist-cil-dev libzeitgeist-dev libzeitgeist-doc libmate-sensors-applet-plugin0 mate-sensors-applet mate-sensors-applet-common apport-gtk apport-symptoms libimage-sane-perl libffmpegthumbnailer4v5 ffmpegthumbnailer rfkill libhpmud0 mate-applets
TempOutOfBoundries 55000 20 shutdown 10
sudo echo -e "$(date '+%F %T') $(($(date '+%s') - $start))s:   Start purging process (4/5)" | tee -a "$installog"
sudo apt purge --remove --yes python3-netifaces mate-applets-common libcpufreq0 ubuntu-minimal xserver-xorg-input-synaptics xserver-xorg-input-synaptics-dev librarian0 rarian-compat gnome-themes-standard gnome-themes-standard-data xarchiver file-roller p0f pmount nautilus-extension-brasero media-player-info libsnmp30 lm-sensors pcmciautils ureadahead librsync1 vlc-plugin-video-splitter a11y-profile-manager-indicator vlc-plugin-visualization golang-github-ubuntu-ubuntu-report-dev libnss-resolve cpuset libcpuset-dev libcpuset1 php7.2-enchant python3-enchant
TempOutOfBoundries 55000 20 shutdown 10
sudo echo -e "$(date '+%F %T') $(($(date '+%s') - $start))s:   Start purging process (5/5)" | tee -a "$installog"
sudo apt purge --remove --yes aspell libspeechd2 speech-dispatcher speech-dispatcher-audio-plugins speech-dispatcher-espeak-ng python3-speechd espeak-ng-data libespeak-ng1 libbrlapi0.6 xserver-xorg-input-wacom python3-speechd libspeechd2 cups cups-ppdc libnma0 #caja
TempOutOfBoundries 55000 20 shutdown 10
sudo echo -e "$(date '+%F %T') $(($(date '+%s') - $start))s:   Packages purged" >> $installog

sudo killall ubuntu-mate-welcome
sudo echo -e "$(date '+%F %T') $(($(date '+%s') - $start))s:   Ubuntu MATE Welcome killed" >> $installog
sudo snap remove ubuntu-mate-welcome software-boutique
sudo echo -e "$(date '+%F %T') $(($(date '+%s') - $start))s:   Welcome and Software Boutique removed" >> $installog
sudo apt remove bubblewrap -y && sudo apt autoremove -y
sudo echo -e "$(date '+%F %T') $(($(date '+%s') - $start))s:   Bubblewrap removed and autoremoved packages" >> $installog

TempOutOfBoundries 57000 20 shutdown 10




#
# Install Packages 
#


# Setup firefox
rm -r .config/autostart/
mkdir .config/autostart
cp -f /usr/share/applications/firefox.desktop .config/autostart/
tar -xvzf bms.tar.gz bms/.mozilla -C ~
cp -rf bms/.mozilla .mozilla
rm -r bms
sudo echo -e "$(date '+%F %T') $(($(date '+%s') - $start))s:   Mozilla Firefox profile extracted and copied. Installing Ubuntu Restricted Extras..." >> $installog

TempOutOfBoundries 57000 20 shutdown 10

# Install restricted extras with MS compatibilities
#
# MS forces to accept a license agreement, this blocks the installation script unattended.
# The strategy:
# (1) deploy mechanism for bypassing the license dialog
# (2) install the package as usual, the license dialog will be hidden.
# See: http://askubuntu.com/questions/16225/how-can-i-accept-the-agreement-in-a-terminal-like-for-ttf-mscorefonts-installer
# See: https://askubuntu.com/questions/463754/how-to-make-ttf-mscorefonts-installer-package-download-fonts-after-it-says-it-i/463760
sudo /bin/sh -c 'echo ttf-mscorefonts-installer msttcorefonts/accepted-mscorefonts-eula select true | debconf-set-selections'
sudo rm -rf /var/lib/update-notifier/package-data-downloads/partial/*
sudo apt-get --purge --reinstall install ttf-mscorefonts-installer -y
sudo apt install ubuntu-restricted-extras -y
sudo echo -e "$(date '+%F %T') $(($(date '+%s') - $start))s:   Ubuntu Restricted Extras installed" >> $installog

# Install Bluetooth for Raspberry PI
#sudo apt install pi-bluetooth -y

# Install SSH
sudo apt install ssh openssh-server openssl openvpn -y
sudo echo -e "$(date '+%F %T') $(($(date '+%s') - $start))s:   SSH installed" >> $installog

TempOutOfBoundries 55000 20 shutdown 10




#
# Install localhost
#


# Install localhost
sudo echo -e "$(date '+%F %T') $(($(date '+%s') - $start))s:   Installing localhost..." >> $installog
sudo apt install apache2-bin apache2 -y
sudo echo -e "$(date '+%F %T') $(($(date '+%s') - $start))s:   Apache 2 installed" >> $installog
sudo apt install mysql-server mysql-client -y
sudo echo -e "$(date '+%F %T') $(($(date '+%s') - $start))s:   MySQL server and client installed, Installing PHP packages..." >> $installog
#sudo apt install php7.3 php7.3-mysql libapache2-mod-php7.3 -y
sudo apt install php7.2 php7.2-mysql libapache2-mod-php7.2 -y

TempOutOfBoundries 55000 20 shutdown 10

# Install PHP packages and modules
#sudo apt install php7.1-mapi php7.2-enchant php7.2-sqlite3 php7.2-fpm php7.2-odbc php7.2-sybase php7.2-bcmath php7.2-gd php7.2-tidy php7.2-bz2 php7.2-gmp php7.2-pgsql php7.2-xml php7.2-cgi php7.2-imap php7.2-phpdbg php7.2-xmlrpc php7.2-interbase php7.2-pspell php7.2-xsl php7.2-intl php7.2-zip php7.2-curl php7.2-recode php7cc php7.2-dba php7.2-ldap php7.2-snmp php7.2-dev php7.2-mbstring php7.2-soap -y
sudo apt install php7.1-mapi php7.2-enchant php7.2-fpm php7.2-odbc php7.2-sybase php7.2-bcmath php7.2-gd php7.2-tidy php7.2-bz2 php7.2-gmp php7.2-xml php7.2-cgi php7.2-imap php7.2-phpdbg php7.2-xmlrpc php7.2-interbase php7.2-pspell php7.2-xsl php7.2-intl php7.2-zip php7.2-curl php7.2-recode php7cc php7.2-dba php7.2-ldap php7.2-snmp php7.2-mbstring -y
sudo apt install php-pear -y
sudo echo -e "$(date '+%F %T') $(($(date '+%s') - $start))s:   PHP packages installed" >> $installog

TempOutOfBoundries 57000 20 shutdown 10

# Install Jquery
sudo apt install libjs-jquery -y
sudo echo -e "$(date '+%F %T') $(($(date '+%s') - $start))s:   Jquery installed" >> $installog
# Restart Apache
sudo a2enmod proxy_fcgi setenvif
sudo a2enconf php7.2-fpm
systemctl restart apache2
sudo echo -e "$(date '+%F %T') $(($(date '+%s') - $start))s:   Apache2 has restarted for implementing newly installed modules" >> $installog

# Install packages for testing
# midnight commander, dconf editor, phpmyadmin
#sudo apt install mc dconf-editor phpmyadmin -y

sudo apt update && sudo apt dist-upgrade -y && sudo apt autoremove -y && sudo apt autoclean
sudo echo -e "$(date '+%F %T') $(($(date '+%s') - $start))s:   System updated, upgraded and cleaned once again" >> $installog
TempOutOfBoundries 57000 20 shutdown 10

# Extract BMS C-application
sudo mkdir /usr/share/bms/
sudo tar -xvzf bms.tar.gz bms/app/ -C ~
sudo cp -rf bms/* /usr/share/bms/
sudo echo -e "$(date '+%F %T') $(($(date '+%s') - $start))s:   BMS C-application extracted and installed" >> $installog

# See: https://www.cyberciti.biz/faq/linux-unix-copy-a-file-to-multiple-directories-using-cp-command/
sudo xargs -n 1 cp -v bms/app/bms.desktop<<<"/usr/share/applications/ ${HOME}/.config/autostart/"
sudo chown $(whoami):$(getent group $(whoami) | awk -F: '{print $1}') .config/autostart/bms.desktop
sudo rm -r bms
sudo echo -e "$(date '+%F %T') $(($(date '+%s') - $start))s:   Auto start BMS C-application set" >> $installog

# Extract BMS web-application
# Note: Directory becomes available after installing Apache 2
cd /var/www/html
sudo rm index.html
sudo tar -xvzf ~/bms.tar.gz bms/web/ -C ./
sudo cp -rf bms/web/* ./
sudo rm -r bms/web
sudo echo -e "$(date '+%F %T') $(($(date '+%s') - $start))s:   BMS web application extracted and installed" >> $installog

# Download Jquery file
sudo mkdir bms/jquery
sudo echo -e "$(date '+%F %T') $(($(date '+%s') - $start))s:   Downloading Jquery 3.4.0 file... (See log file: $wgetlog/jquery.log)" | tee -a "$installog"
wget -o $wgetlog/jquery.log http://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js -O bms/jquery/jquery.min.js --tries=3


# See: https://www.digitalocean.com/community/tutorials/how-to-install-mysql-on-ubuntu-18-04
#sudo mysql_secure_installation
# Type "y" (Validate Password Plugin)
# Type "2" (STRONG)
# Type new password
# Re-enter new password
# Type "y" (Continue?)
# Type "y" (Remove anonymous users?)
# Type "y" (Disallow root login remotely?)
# Type "y" (Remove test database and access to it?)
# Type "y" (Reload privilege tables now?)
#
# All of the above options in mysql_secure_installation will be replaced by the following
# See: https://gist.github.com/Mins/4602864

echo -e "\n\nBash script finishes"
echo "Enter a strong and secure MySQL root password, followed by [ENTER]: "

# Hide MySQL password
# See: https://stackoverflow.com/questions/8354777/how-i-can-make-read-doesnt-show-its-value-in-shell-scripting
stty -echo
read DATABASE_PASS
stty echo

# MySQL commands from Linux Shell see:
# See: https://www.shellhacks.com/mysql-run-query-bash-script-linux-command-line/
# $ mysql -u USER -pPASSWORD -e "SQL_QUERY"
#sudo mysqladmin -u root password "$DATABASE_PASS"
echo "Set MySQL root password"
#sudo mysql -u root -p"$DATABASE_PASS" -e "UPDATE mysql.user SET Password=PASSWORD('$DATABASE_PASS') WHERE User='root'"
sudo mysql -u root -p"$DATABASE_PASS" -e "ALTER USER 'root'@'localhost' IDENTIFIED WITH mysql_native_password BY '$DATABASE_PASS'"
sudo echo -e "$(date '+%F %T') $(($(date '+%s') - $start))s:   MySQL root password set" >> $installog
echo "Set MySQL security"
sudo mysql -u root -p"$DATABASE_PASS" -e "DELETE FROM mysql.user WHERE User='root' AND Host NOT IN ('localhost', '127.0.0.1', '::1')"
sudo mysql -u root -p"$DATABASE_PASS" -e "DELETE FROM mysql.user WHERE User=''"
sudo mysql -u root -p"$DATABASE_PASS" -e "DELETE FROM mysql.db WHERE Db='test' OR Db='test\_%'"
sudo mysql -u root -p"$DATABASE_PASS" -e "FLUSH PRIVILEGES"
sudo echo -e "$(date '+%F %T') $(($(date '+%s') - $start))s:   MySQL security set" >> $installog
# Set MySQL Create MySQL Database
echo "Create database"
sudo mysql -u root -p"$DATABASE_PASS" -e "CREATE DATABASE bms_db"
sudo echo -e "$(date '+%F %T') $(($(date '+%s') - $start))s:   BMS Database created" >> $installog
cd ..

TempOutOfBoundries 59000 20 shutdown 10

# Change ownership and rights of intranet
# See: https://www.2daygeek.com/how-to-check-which-groups-a-user-belongs-to-on-linux/
sudo echo -e "$(date '+%F %T') $(($(date '+%s') - $start))s:   Adapting user groups and rights in localhost" | tee -a "$installog"
sudo chown -R $(whoami):$(getent group $(whoami) | awk -F: '{print $1}') ./
sudo find ./ -type f -exec chmod -c 644 {} \; | tee -a "$installog"
sudo find ./ -type d -exec chmod -c 755 {} \; | tee -a "$installog"
sudo echo -e "$(date '+%F %T') $(($(date '+%s') - $start))s:   Localhost file structure permissions set" >> $installog

# Load MySQL file that fills the database
echo "Load database from file"
sudo mysql -u root -p"$DATABASE_PASS" < html/bms/sql/bms.sql
sudo echo -e "$(date '+%F %T') $(($(date '+%s') - $start))s:   Database loaded from html/bms/sql/bms.sql, database tables:" >> $installog
sleep 10 # Wait to finish MySQL
# Show MySQL BMS database tables as a test
sudo mysql -u root -p"$DATABASE_PASS" -e "USE bms_db; SHOW TABLES" | tee -a "$installog"




#
# Set cron tasks
#
# See: https://tecadmin.net/crontab-in-linux-with-20-examples-of-cron-schedule/
# Tutorial: https://www.youtube.com/watch?v=ljgvo2jM234
# Never change: /var/spool/cron/crontabs/bms
# List of crontabs by user:
# sudo crontab -u [user] -l
#
sudo /bin/sh -c 'echo -e "$(whoami)" > /etc/cron.allow'


mkdir $appdir/backup
mkdir $appdir/crontab
mkdir $logdir/crontab
sudo echo -e "$(date '+%F %T') $(($(date '+%s') - $start))s:   Directories for backup and crontab made" >> $installog
# Update software (Ubuntu from online archive & $(whoami) from Git-repo)
#
# Backup MySQL Database
# See: https://www.shellhacks.com/backup-mysql-database-command-line/
# See: https://stackoverflow.com/questions/27593259/does-crontab-take-command-line-arguments
#sudo mysqldump --opt -u root -p"$DATABASE_PASS" bms_db > bms.sql
#sudo crontab -u root @daily $appdir/crontab/backup-sql.sh "$DATABASE_PASS" >> $logdir/crontab/backup-sql.log 2>&1
sudo echo -e "#!/bin/bash\n
sudo mysqldump --opt -u root -p\$1 bms_db > $appdir/backup/bms.sql" >> $appdir/crontab/backup-sql.sh
# See: https://www.linux.com/tutorials/understanding-linux-file-permissions/
sudo chmod +x $appdir/crontab/backup-sql.sh
sudo /bin/sh -c 'echo "59 2\t* * *\t$(whoami)    $appdir/crontab/backup-sql.sh $DATABASE_PASS >> $logdir/crontab/backup-sql.log 2>&1" >> /etc/crontab'





#
# Configure gsettings 
#


# Autologin user
if [ ! -f "$lightdmgreeter" ]; then
    sudo /bin/sh -c "echo '[SeatDefaults]
    greeter-session=lightdm-gtk-greeter
    autologin-user=$(whoami)' | tee -a 60-lightdm-gtk-greeter.conf > '$lightdmgreeter'"
    if [ ! -f "$lightdmgreeter" ]; then
        sudo echo -e "$(date '+%F %T') $(($(date '+%s') - $start))s:   Auto login failed to set for $(whoami)" | tee -a "$installog"
    else
        sudo echo -e "$(date '+%F %T') $(($(date '+%s') - $start))s:   Auto login successfully set for $(whoami)" | tee -a "$installog"
    fi
fi

# Archive extracted and its content found its destination: job done!
sudo rm ~/bms.tar.gz
sudo echo -e "$(date '+%F %T') $(($(date '+%s') - $start))s:   BMS archive removed" >> $installog

echo "Setting background..."
sudo rm /usr/share/backgrounds/ubuntu-mate-common/*.*
gsettings set org.mate.background picture-filename ''
gsettings set org.mate.background picture-options 'wallpaper'
gsettings set org.mate.background color-shading-type 'solid'
gsettings set org.gnome.desktop.background primary-color '#121414'
gsettings set org.mate.background primary-color 'rgb(18,20,20)'
gsettings set org.mate.background secondary-color 'rgb(18,20,20)'
sudo echo -e "$(date '+%F %T') $(($(date '+%s') - $start))s:   Background set" >> $installog

echo "Disable some animations..."
# See: https://ubuntu-mate.community/t/boot-into-the-terminal-then-launch-the-mate-desktop/15923/3
gsettings set org.gnome.desktop.interface enable-animations false
gsettings set org.mate.interface gtk-enable-animations false
gsettings set org.mate.panel enable-animations false
sudo echo -e "$(date '+%F %T') $(($(date '+%s') - $start))s:   Animations removed" >> $installog

echo "Removing icons..."
gsettings set org.mate.hud enabled true
gsettings set org.mate.background show-desktop-icons false # Disables /home
gsettings set org.mate.Marco.general allow-tiling false
gsettings set org.mate.caja.desktop home-icon-visible false
gsettings set org.mate.mate-menu plugins-list '[]'
sudo echo -e "$(date '+%F %T') $(($(date '+%s') - $start))s:   Icons removed" >> $installog

# Delete and clear panels
echo "Delete and clear panels..."
gsettings set org.mate.panel object-id-list '[]'
gsettings set org.mate.panel.menubar show-applications false
gsettings set org.mate.panel.menubar show-desktop false
sudo echo -e "$(date '+%F %T') $(($(date '+%s') - $start))s:   Panels deleted and cleared" >> $installog

# Disable auto-mount
echo "Disable automount..."
gsettings set org.mate.media-handling automount-open false
gsettings set org.mate.media-handling automount false
gsettings set org.gnome.desktop.media-handling automount-open false
gsettings set org.gnome.desktop.media-handling automount false
sudo echo -e "$(date '+%F %T') $(($(date '+%s') - $start))s:   Automount disabled" >> $installog

# Disable Trash
echo "Disable trash..."
gsettings set org.mate.caja.preferences confirm-trash false
gsettings set org.mate.mate-menu.plugins.places show-trash false
sudo echo -e "$(date '+%F %T') $(($(date '+%s') - $start))s:   Trash option disabled" >> $installog

echo "Disable sounds..."
# See: https://askubuntu.com/questions/557389/how-can-i-disable-all-ubuntu-sounds/849757
gsettings set org.gnome.desktop.sound event-sounds false
gsettings set org.mate.sound event-sounds true
gsettings set org.mate.sound input-feedback-sounds false
gsettings set org.mate.sound theme-name 'ubuntu'
sudo echo -e "$(date '+%F %T') $(($(date '+%s') - $start))s:   Sounds disabled" >> $installog

echo "Change other settings..."
gsettings set com.ubuntu.update-notifier no-show-notifications true
sudo echo -e "$(date '+%F %T') $(($(date '+%s') - $start))s:   Notify updates disabled" >> $installog
#gsettings set org.mate.terminal.profile scrollback-unlimited true # Error: Schema "org.mate.terminal.profile" is relocatable (path must be specified)

# Enable suspend
echo "Suspend settings..."
sudo systemctl unmask sleep.target suspend.target hibernate.target hybrid-sleep.target
gsettings set org.mate.screensaver lock-enabled true
gsettings set org.mate.screensaver idle-activation-enabled true
gsettings set org.mate.session idle-delay 5
gsettings set org.mate.session logout-prompt false
gsettings set org.mate.session logout-timeout 0
gsettings set org.gnome.desktop.lockdown disable-lock-screen false
gsettings set org.gnome.desktop.screensaver lock-enabled false
gsettings set org.gnome.desktop.screensaver idle-activation-enabled true
gsettings set org.gnome.desktop.screensaver ubuntu-lock-on-suspend true
gsettings set org.gnome.desktop.session idle-delay 300
sudo echo -e "$(date '+%F %T') $(($(date '+%s') - $start))s:   Suspend enabled" >> $installog

echo "Lock down system..."
gsettings set org.mate.lockdown disable-lock-screen false
#gsettings set org.mate.lockdown disable-log-out true
gsettings set org.mate.lockdown disable-printing true
gsettings set org.mate.lockdown disable-print-setup true
gsettings set org.mate.lockdown disable-theme-settings true
gsettings set org.mate.lockdown disable-user-switching true
#gsettings set org.mate.lockdown user-administration-disabled true
gsettings get org.gnome.desktop.lockdown disable-lock-screen false
#gsettings set org.gnome.desktop.lockdown disable-log-out true
gsettings set org.gnome.desktop.lockdown disable-printing true
gsettings set org.gnome.desktop.lockdown disable-print-setup true
gsettings set org.gnome.desktop.lockdown disable-user-switching true
#gsettings set org.gnome.desktop.lockdown user-administration-disabled true
sudo echo -e "$(date '+%F %T') $(($(date '+%s') - $start))s:   System locked down" >> $installog

TempOutOfBoundries 57000 20 shutdown 10

# See: https://unix.stackexchange.com/questions/387010/bash-calculate-the-time-elapsed-between-two-timestamps
end=$(date '+%s')
delta=$((end - start))
sudo echo -e "$(date '+%F %T') $(($(date '+%s') - $start))s:   Bash is finished and it took $(($delta / 60)) minutes! Press [ENTER] to reboot..." | tee -a "$installog"
read

# See: https://www.cyberciti.biz/faq/linux-logout-user-howto/
#sudo pkill -KILL -u bms
sudo echo -e "$(date '+%F %T') $(($(date '+%s') - $start))s:   System reboot\n\n\n" >> $installog
sudo reboot now
# -- End of BASH Script --
