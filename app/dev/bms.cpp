#include "./classes/bms_class.h"

int main()
{
	string mysql_server = "localhost";
	string mysql_username = "root";
	string mysql_password = "master"; //"BMS1-master"
	string mysql_database = "bms_db";

	Mysql *Database = new Mysql(mysql_server, mysql_username, mysql_password, mysql_database);
	if(!Database->Connect()) {
		cout << Database->GetError() << endl;
		return FAILURE;
	}

	string mysql_measurements_table = "measurements";
	if(Database->TableExists(mysql_measurements_table) < 0) {
		Logbook lb(Database);
	 	lb.Add(1, "Table", "No table found with the name \"" + mysql_measurements_table + "\"", 1);
		return FAILURE;
	}


	vector <int> epds;
	epds.push_back(250);
	epds.push_back(238);
	epds.push_back(227);
	epds.push_back(216);
	epds.push_back(205);
	epds.push_back(190);
	epds.push_back(173);
	epds.push_back(156);
	epds.push_back(139);
	epds.push_back(122);
	epds.push_back(104);
	epds.push_back(92);
	epds.push_back(77);
	epds.push_back(52);
	epds.push_back(33);
	epds.push_back(12);

	BatteryCells *BattPack = new BatteryCells(Database);

	BATTERYCELL cellstatus;
	cellstatus.id = 0;
	cellstatus.epd_id = 130;
	cellstatus.active = 1;
	cellstatus.mounted = time(0);

	string mysql_cells_table = "cells";
	vector <string> cell_fieldnames, measurement_fieldnames, subjects, exclude;
	if(!Database->GetFieldNames(mysql_cells_table, cell_fieldnames, exclude)) {
		Logbook lb(Database);
	 	lb.Add(1, "Battery cells", "Failed to get database fields. " + Database->GetError() + "", 1);
		return FAILURE;
	}

	string query = "DELETE FROM `" + mysql_measurements_table + "`";
	if(Database->Query(query)) {
		Database->FreeResult();
	} else {
		Logbook lb(Database);
	 	lb.Add(4, "Delete measurements", Database->GetError(), 1);
	}
	query = "DELETE FROM `" + mysql_cells_table + "`";
	if(Database->Query(query)) {
		Database->FreeResult();
	} else {
		Logbook lb(Database);
	 	lb.Add(4, "Delete battery cells", Database->GetError(), 1);
	}
	query = "DELETE FROM `logs`";
	if(Database->Query(query)) {
		Database->FreeResult();
	} else {
		Logbook lb(Database);
	 	lb.Add(4, "Delete log entries", Database->GetError(), 1);
	}

	exclude.clear();
	exclude.push_back("id");
	if(!Database->GetFieldNames(mysql_measurements_table, measurement_fieldnames, exclude)) {
		Logbook lb(Database);
	 	lb.Add(1, "Measurements", "Failed to get database fields. " + Database->GetError() + "", 1);
		return FAILURE;
	}

	exclude.push_back("cell");
	exclude.push_back("time");
	if(!Database->GetFieldNames(mysql_measurements_table, subjects, exclude)) {
		Logbook lb(Database);
	 	lb.Add(1, "Battery cells", "Failed to get database fields. " + Database->GetError() + "", 1);
		return FAILURE;
	}
	Settings settings(Database, 1);
	BatteryCell *cell = new BatteryCell(cellstatus, settings, mysql_cells_table, cell_fieldnames);

	if(!BattPack->Add(cell)) {
		Logbook lb(Database);
	 	lb.Add(1, "Battery cell", "Failed to add battery cell" + (cellstatus.id > 0 ? "\"" + StringPatch::ToString(cellstatus.id) + "\"" : ""), 1);
		return FAILURE;
	}

	/* Received data */
	const int NumberOfMeasurements = 1;
	vector <MEASUREMENT> receiveddata;
	MEASUREMENT data[1];
	data[0].timestamp = 1572347386;
	data[0].epd = 412;
	data[0].current = 180;
	data[0].temp_cell = 52;
	data[0].temp_mproc = 36;

	for(int i = 0; i < NumberOfMeasurements; i++) {
		receiveddata.push_back(data[i]);
	}

	BatteryCell LastMountedCell;
	if(!BattPack->GetLatestMountedCell(LastMountedCell)) {
		Logbook lb(Database);
	 	lb.Add(1, "Battery cell", "Failed to get ID", 1);
		return FAILURE;
	}
	cout << LastMountedCell.GetId() << endl;
/*	Measurement *measurement = new Measurement(cell_id, measurement_fieldnames, receiveddata);
	if(!measurement->GetError().empty()) {
		Logbook lb(Database);
	 	lb.Add(1, "Measurements", measurement->GetError(), 1);
		return FAILURE;
	}

//	DataTable m(measurement->GetFields(), measurement->GetMeasurementValues(), mysql_measurements_table);
//	cout << m.GetValuesString() << endl;

	if(!Database->InsertValues(mysql_measurements_table, measurement->GetFields(), measurement->GetMeasurementValues(), 1)) {
		Logbook lb(Database);
	 	lb.Add(1, "Measurements", "Failed to add measurement: " + Database->GetError(), 1);
		return FAILURE;
	}

	Database->FreeResult();

	measurement->Print();
	delete measurement;
*/
	return SUCCESS;
}
