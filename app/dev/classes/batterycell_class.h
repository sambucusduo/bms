#ifndef __BATTERYCELL_CLASS_H_
#define __BATTERYCELL_CLASS_H_

#include "logbook_class.h"
#include "settings_class.h"

enum CURRENT_STATUS {	
	EXPENSING = -1,
	NUETRAL   = 0,
	CHARGING  = 1
};

struct BATTERYCELL
{
	unsigned short int id;
	unsigned short int epd_id;
	unsigned short int active;
				time_t mounted;
};
/* Battery "condition", "operation" and "age" can be processed */


class BatteryCell: public Settings
{
private:
	BATTERYCELL _battery_cell;

	string _tableName;
    vector <string> _fieldNames;

	Settings _settings;
	CURRENT_STATUS _status;
public:
	/* Constructor */
	BatteryCell();
	BatteryCell(BATTERYCELL, Settings, string, vector <string>);
	/* Destructor */
	~BatteryCell(void);

	string GetTableName(void);
    vector <string> GetFields(void);
	unsigned short int GetId(void);
	unsigned short int GetEpdId(void);
	unsigned short int GetActive(void);
	time_t GetMounted(void);

	string GetIdStr(void);
	string GetEpdIdStr(void);
	string GetConditionStr(void);
	string GetActiveStr(void);
	string GetMountedStr(void);
	string GetSettingStr(void);
//	vector <string> GetValues(unsigned int);
	vector <string> GetValues(void);

	void SetId(unsigned int);
};

/* private */

/* public */

/* Constructor */
BatteryCell::BatteryCell()
{

}

/* Constructor */
BatteryCell::BatteryCell(BATTERYCELL bc, Settings settings, string tablename, vector <string> fieldnames)
{
	_battery_cell = bc;
	_settings = settings;
	_tableName = tablename;
    _fieldNames = fieldnames;
}

/* Destructor */
BatteryCell::~BatteryCell(void)
{
	
}

/* Gets table name */
string BatteryCell::GetTableName(void)
{
	return _tableName;
}

/* Gets names of data table columns */
vector <string> BatteryCell::GetFields(void)
{
    return _fieldNames;
}

/* Gets cell ID */
unsigned short int BatteryCell::GetId(void)
{
	return _battery_cell.id;
}

/* Gets cell distant in epd */
unsigned short int BatteryCell::GetEpdId(void)
{
	return _battery_cell.epd_id;
}

/* Gets the active cell status */
unsigned short int BatteryCell::GetActive(void)
{
	return  _battery_cell.active;
}

/* Gets the date of cell mounted */
time_t BatteryCell::GetMounted(void)
{
	return _battery_cell.mounted;
}

/* Gets the cell ID as string type */
string BatteryCell::GetIdStr(void)
{
	return _battery_cell.id == 0 ? "" : StringPatch::ToString(_battery_cell.id);
}

/* Gets the cell distant as string type */
string BatteryCell::GetEpdIdStr(void)
{
	return StringPatch::ToString(_battery_cell.epd_id);
}

/* Gets the active cell status as string type */
string BatteryCell::GetActiveStr(void)
{
	return  StringPatch::ToString(_battery_cell.active);
}

/* Gets the mounted time as string type */
string BatteryCell::GetMountedStr(void)
{
	return Mysql::UnixTimeToSQLDate(_battery_cell.mounted);
}

/* Gets setting */
string BatteryCell::GetSettingStr(void)
{
	return StringPatch::ToString(_settings.GetSettingId());
}

/* Gets the data of the cells data as an imploded string */
//vector <string> BatteryCell::GetValues(unsigned int availableId = 0)
vector <string> BatteryCell::GetValues(void)
{
	vector <string> values;

//	values.push_back(GetIdStr().empty() ? StringPatch::ToString(availableId) : GetIdStr());
	values.push_back(GetIdStr().empty() ? "0" : GetIdStr()); //LAST_INSERT_ID()
	values.push_back(GetEpdIdStr());
	values.push_back(GetActiveStr());
	values.push_back(GetMountedStr());
	values.push_back(GetSettingStr());

	return values;
}

/* Sets Id - Use ONLY when Id equals 0 and use sparingly with caution! */
void BatteryCell::SetId(unsigned int id)
{
	if(_battery_cell.id == 0) {
		_battery_cell.id = id;
	}
}

#endif