#ifndef __BMS_CLASS_H_
#define __BMS_CLASS_H_

#include "measurement_class.h"

class BMS: public Mysql
{
private:
    Mysql *_Database;
    BatteryCells *_BatteryPack;
public:
    /* Constructor */
    BMS(string, string, string, string);
    /* Destructor */
    ~BMS();

    int ConnectWithDatabase(void);
    int TableExists(string);
    int GetFieldNames(string, vector <string> &, vector <string>);
    int ClearTable(string);
    int AddBatteryCell(int, unsigned int, vector <string>);
	void SyncClock(void);
    int ReceiveMeasurements(vector <string>);
};

/* private */

/* public */

/* Constructor */
BMS::BMS(string mysql_server, string mysql_username, string mysql_password, string mysql_database)
{
	_Database = new Mysql(mysql_server, mysql_username, mysql_password, mysql_database);
}

/* Destructor */
BMS::~BMS()
{
//    if(_BatteryPack->GetNumberOfCells()) {
        delete _BatteryPack;
//    }

    delete _Database;
}

/* Connect with MySQL database */
int BMS::ConnectWithDatabase(void)
{
	return _Database->Connect();
}

/* Checks whether a data table exists */
int BMS::TableExists(string tabelname)
{
	if(_Database->TableExists(tabelname) < 0) {
		Logbook lb(_Database);
	 	lb.Add(1, "Table", "No table found with the name \"" + tabelname + "\"", 1);
		return FAILURE;
	}

    return SUCCESS;
}

/* Gets field names of a data table */
int BMS::GetFieldNames(string tablename, vector <string> &fieldnames, vector <string> exclude)
{
	if(!_Database->GetFieldNames(tablename, fieldnames, exclude)) {
		Logbook lb(_Database);
	 	lb.Add(1, tablename, "Failed to get database fields. " + _Database->GetError() + "", 1);
		return FAILURE;
	}

    return SUCCESS;
}

/* Clears data table */
int BMS::ClearTable(string tablename)
{
	string query = "DELETE FROM `" + tablename + "`";
	if(!_Database->Query(query)) {
        Logbook lb(_Database);
        lb.Add(4, tablename, _Database->GetError(), 1);
        return FAILURE;
	}

	_Database->FreeResult();

    return SUCCESS;
}

/* Add battery cell */
int BMS::AddBatteryCell(int epd_id, unsigned int set, vector <string> fieldnames)
{
	BATTERYCELL cellstatus;
	cellstatus.id = 0;
	cellstatus.epd_id = epd_id;
	cellstatus.active = 1;

    BatteryCell *batterycell = new BatteryCell(cellstatus, Settings(_Database, set), string("cells"), fieldnames);

	if(!_BatteryPack->Add(batterycell)) {
		Logbook lb(_Database);
	 	lb.Add(1, "Battery cell", "Failed to add battery cell \"" + StringPatch::ToString(cellstatus.id) + "\"", 1);
		return FAILURE;
	}

	BatteryCell LastMountedCell;
	int cellid = _BatteryPack->GetLatestMountedCell(LastMountedCell);//.GetId();
    return cellid;
}

/* Sends a date for all cell modules to synchronise with */
void BMS::SyncClock(void)
{
	time_t synctime = time(0);
}

/* Gets measurements form the cell modules and adds the measurement in the database */
int BMS::ReceiveMeasurements(vector <string> fieldnames)
{
	string tablename = "measurements";

   	vector <MEASUREMENT> receiveddata;
	MEASUREMENT data[1];
	data[0].timestamp = 1572347386;
	data[0].epd = 412;
	data[0].current = 180;
	data[0].temp_cell = 52;
	data[0].temp_mproc = 36;

	Measurement *measurement = new Measurement(1, fieldnames, receiveddata);
	if(!measurement->GetError().empty()) {
		Logbook lb(_Database);
	 	lb.Add(1, tablename, measurement->GetError(), 1);
		return FAILURE;
	}

	measurement->Print();
	DataTable m(measurement->GetFields(), measurement->GetMeasurementValues(), tablename);
//	cout << m.GetValuesString() << endl;

	_Database->InsertValues(tablename, measurement->GetFields(), measurement->GetMeasurementValues());
	_Database->FreeResult();

	return SUCCESS;
}

#endif