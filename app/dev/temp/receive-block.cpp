int getBlock2(char buf[], int Toutms, int maxbytes) {
    if(maxbytes>=sizeof (buf)) return -1;
    int bytes = 0;
    int oldbytes = 0;
    struct timespec pause;
    pause.tv_sec = 0;
    pause.tv_nsec = Toutms * 1000;
    for (;;) {
        oldbytes = bytes;
        if (bytes >= maxbytes) break;
        nanosleep(&pause, NULL);
        ioctl(sfd, FIONREAD, &bytes);
        if (oldbytes == bytes)break;
    }
    memset(buf, '\0', sizeof (buf));
    if (bytes >= maxbytes)bytes=maxbytes;
    int count = read(sfd, buf, bytes);
    buf[count+1] = 0;
    return count;
}