#ifndef __MYSQL_CLASS_H_
#define __MYSQL_CLASS_H_

#include "mysql_table_class.h"
#include <time.h>

#define FAILURE 0
#define SUCCESS 1


class Mysql {
private:
	MYSQL *conn;
	MYSQL_RES *result;

	string server;
	string user;
	string password;
	string database;

    vector <string> TableNames;

    string error_message;

	void SetResult(void);
	void GetTableNames(unsigned int);
    string InsertQuery(string, vector <string>, vector <string>);

public:
    /* Constructors */
    Mysql();
	Mysql(string, string, string, string);
    /* Destructors */
	~Mysql(void);
    void Close(void);

    /* database information getters */
	string GetServer(void);
	string GetUser(void);
	string GetDatabase(void);

    /* error methods */
    string GetMysqlError(void);
	string GetError(void);

    /* result methods */
	MYSQL_RES *GetResult(void);
	MYSQL_RES *StoreResult(void);
	void FreeResult(void);
    bool HasResult(void);

    /* database methods */
	int Connect(void);
	MYSQL_ROW FetchRow(void);
	unsigned int NumberOfFields(void);
	unsigned int NumberOfRows(void);
	int Query(string);
	MYSQL_TAB QueryArray(string);

    int TableExists(string, unsigned int);
    int GetFieldNames(string, vector <string> &, vector <string>);
    int InsertValues(string, vector <string>, vector <string>, unsigned int);

    /* static methods */
	static string UnixTimeToSQLDate(time_t);
};

/* private */

/* Sets result */
void Mysql::SetResult(void)
{
    result = mysql_use_result(conn);
}

/* Gets list of data tables */
void Mysql::GetTableNames(unsigned int output = 0)
{
   	Query("SHOW TABLES");

	StoreResult();

    MYSQL_ROW _table;

	while((_table = FetchRow()) > 0) {
        TableNames.push_back(_table[0]);
        if(output) {
            cout << TableNames[TableNames.size()-1] << endl;
        }
	}

    FreeResult();
}


/* public */

/* Constructor */
Mysql::Mysql()
{

}

/* Constructor */
Mysql::Mysql(string svr, string usr, string pwd, string dbs)
{
    server = svr;
    user = usr;
    password = pwd;
    database = dbs;

    conn = mysql_init(NULL);

    if(!error_message.empty()) error_message.clear();
//    result = NULL;
}

/* Destructor */
void Mysql::Close(void)
{
/*    if(HasResult()) {
        mysql_free_result(result);
    }
*/
    mysql_close(conn);
}

/* Destructor */
Mysql::~Mysql(void)
{
    Close();
}

/* Gets server name */
string Mysql::GetServer(void)
{
    return server;
}

/* Gets user name */
string Mysql::GetUser(void)
{
    return user;
}

/* Gets database name */
string Mysql::GetDatabase(void)
{
    return database;
}

/* Gets MySQL Error */
string Mysql::GetMysqlError(void)
{
    string mysqlerror(mysql_error(conn));
    return mysqlerror;
}

/* Gets Error */
string Mysql::GetError(void)
{
    return error_message;
}

/* Gets Result */
MYSQL_RES *Mysql::GetResult(void)
{
    return result;
}

/* Stores result of a data query */
MYSQL_RES *Mysql::StoreResult(void)
{
    return mysql_store_result(conn);
}

/* Free result */
void Mysql::FreeResult(void)
{
    mysql_free_result(result);
}

/* Check whether the class holds a result */
bool Mysql::HasResult(void)
{
    return result != 0;
}

/* Connects with Mysql database */
int Mysql::Connect(void)
{
    if (!mysql_real_connect(conn, server.c_str(), user.c_str(), password.c_str(), database.c_str(), 0, NULL, 0)) {
        error_message = "Failed to connect MySQL Server \"" + server + "\". MySQL says: " + GetMysqlError();
        return FAILURE;
    }

    GetTableNames();

    return SUCCESS;
}

/* Fetches a row of a query result */
MYSQL_ROW Mysql::FetchRow(void)
{
    return mysql_fetch_row(result);
}

/* Counts the number of columns from a result */
unsigned int Mysql::NumberOfFields(void)
{
    return mysql_num_fields(result);
}

/* Counts the number of rows from a result */
unsigned int Mysql::NumberOfRows(void)
{
    return mysql_num_rows(result);
}

/* Check whether a table exists in the database */
int Mysql::TableExists(string tableName, unsigned int offset = 0)
{
    int found = 0;
    unsigned int i = offset, n = TableNames.size();
    while (!found && i < n) {
        found = (tableName == TableNames[i++]);
    }

    return (i < n && found ? i : 0) -1;
}

/* Gets field names of a data table */
int Mysql::GetFieldNames(string tablename, vector <string> &fieldnames, vector <string> exclude)
{
    if(tablename.empty()) {
        error_message = "No table name has been given";
        return FAILURE;
    }

    if(TableExists(tablename) < 0) {
        error_message = "No table found with the name \"" + tablename + "\"";
        return FAILURE;
    }

    string query = "SELECT `COLUMN_NAME` FROM `INFORMATION_SCHEMA`.`COLUMNS`";
    query += " WHERE `TABLE_SCHEMA`='" + database + "' AND `TABLE_NAME`='" + tablename + "'";

    if(unsigned int n = exclude.size()) {
        for(unsigned int i = 0; i < n; i++) {
            query += " AND `COLUMN_NAME` <> '" + exclude[i] + "'";
        }
    }

    if(!Query(query)) {
        return FAILURE;
    }

    MYSQL_ROW field;
    while((field = FetchRow()) != 0) {
        fieldnames.push_back(field[0]);
    }

    FreeResult();

    return SUCCESS;
}

/* Perform a data query */
int Mysql::Query(string query)
{
    if (mysql_query(conn, query.c_str())) {
        error_message = "Failed to execute query. MySQL says: " + GetMysqlError();
        return FAILURE;
    }

    SetResult();

    return SUCCESS;
}

/* Perform a data query and give a table */
MYSQL_TAB Mysql::QueryArray(string query)
{
    if(!Query(query)) {
        return NULL;
    }
    
    MYSQL_TAB table(result);

    return table;
}

/* Prepare a query string for data insertion to a data table */
string Mysql::InsertQuery(string tableName, vector <string> columns, vector <string> values)
{
    if(TableExists(tableName) < 0) {
        error_message = "Table \"" + tableName + "\" not found";
        return NULL;
    }

    if(values.size() == 0) {
        error_message = "Not enough or no values to insert";
        return "";
    }

    DataTable *TableToInsert = new DataTable(columns, values, tableName);

    return "INSERT INTO " + tableName + " " + TableToInsert->GetFieldsString() + " VALUES " + TableToInsert->GetValuesString();
}

/* Insert values to a data table */
int Mysql::InsertValues(string tableName, vector <string> columns, vector <string> values, unsigned int outputQuery = 0)
{
    string query = InsertQuery(tableName, columns, values);

    if(query.empty()) {
        error_message = "Cannot query with an empty query string";
        return FAILURE;
    }

    if(outputQuery) {
        cout << query << endl;
    }

    return Query(query) & GetError().empty() & GetMysqlError().empty();
/*
    -------------------------------------------------------------------------------------
               0        &         0          &         0           =          0
               0        &         0          &         1           =          0
               0        &         1          &         0           =          0
               0        &         1          &         1           =          0
               1        &         0          &         0           =          0
               1        &         0          &         1           =          0
               1        &         1          &         0           =          0
               1        &         1          &         1           =          1 (SUCCESS)
               
*/
}

/* Convert Unix Time to a date time format, Mysql understands */
string Mysql::UnixTimeToSQLDate(time_t unixtime)
{
    const int datelength = 20;
    struct tm *timeinfo;
    time(&unixtime);
    timeinfo = gmtime(&unixtime);

	char date[datelength];
	strftime(date, datelength, "%F %T", timeinfo);

    string datestring(date);
    return datestring;
}

#endif
